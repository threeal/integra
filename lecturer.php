<?php
session_start();
 
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once "config.php";

if ($_SESSION["access"] == "lecturer") {

    $sql = "SELECT l.employee_id, l.name FROM lecturers AS l
    WHERE l.user_index IN ( SELECT u.index FROM users AS u WHERE u.username = ? );";

    if ($stmt = mysqli_prepare($connection, $sql)) {
        
        mysqli_stmt_bind_param($stmt, "s", $param_username);
        $param_username = $_SESSION["username"];

        if (mysqli_stmt_execute($stmt)) {
            
            mysqli_stmt_store_result($stmt);
            if (mysqli_stmt_num_rows($stmt) == 1) {

                mysqli_stmt_bind_result($stmt, $db_employee_id, $db_name);
                if (mysqli_stmt_fetch($stmt)) {
                    $_SESSION["employee_id"] = $db_employee_id;
                    $_SESSION["name"] = $db_name;
                }
            } else {
                $_SESSION["loggedin"] = false;
                header("location: login.php");
                exit;
            }
        } else {
            $_SESSION["loggedin"] = false;
            header("location: login.php");
            exit;
        }

        mysqli_stmt_close($stmt);
    } else {
        $_SESSION["loggedin"] = false;
        header("location: login.php");
        exit;
    }

} else {
    if ($_SESSION["access"] == "staff") {
        header("location: staff.php");
    } else if ($_SESSION["access"] == "student") {
        header("location: student.php");
    } else if ($_SESSION["access"] == "parent") {
        header("location: parent.php");
        exit;
    } else {
        $_SESSION["loggedin"] = false;
        header("location: login.php");
    }
    exit;
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lecturer Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body { font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div class="page-header">
        <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["name"]); ?></b>.</h1>
        <h3>NIP. <?php echo htmlspecialchars($_SESSION["employee_id"])?> </h3>
    </div>
    <p>
        <a href="lecturer.php" class="btn btn-danger">View Schedules</a>
        <a href="logout.php" class="btn btn-danger">Sign Out</a>
    </p>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    

                    <?php
                    require_once "config.php";
                    
                    if (isset($_GET["subject_code"])) {

                        $sql = "SELECT
                        sch.student_id AS 'student_id',
                        sch.student_name AS 'student_name'
                        FROM
                        integra.student_schedules_view AS sch,
                        integra.lecturer_schedules_view AS lch
                        WHERE
                        lch.employee_id = ? AND
                        sch.subject_code = ? AND
                        sch.subject_code = lch.subject_code AND
                        sch.class_code = lch.class_code;";

                        if ($stmt = mysqli_prepare($connection, $sql)) {
                                
                            mysqli_stmt_bind_param($stmt, "ss", $param_employee_id, $param_subject_code);
                            $param_employee_id = $_SESSION["employee_id"];
                            $param_subject_code = $_GET["subject_code"];

                            if (mysqli_stmt_execute($stmt)) {
                                
                                mysqli_stmt_store_result($stmt);
                                mysqli_stmt_bind_result($stmt, $db_student_id, $db_student_name);
                                
                                echo '<thead> <tr>
                                    <th> Student ID </th>
                                    <th> Student Name </th>
                                    <th> </th>
                                </tr> </thead>';

                                echo '<tbody>';
                                while (mysqli_stmt_fetch($stmt)) {
                                    echo '<tr>';
                                    echo '<th>';
                                    echo $db_student_id;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_student_name;
                                    echo '</th>';
                                    echo '</tr>';
                                }
                                echo '</tbody>';
                            } else {
                                echo "Oops! Something went wrong. Please try again later.";
                            }
                            mysqli_stmt_close($stmt);
                        } else {
                            echo "Oops! Something went wrong. Please try again later.";
                        }
                    }
                    else {

                        $sql = "SELECT l.subject_code, l.subject_name, l.class_code, l.class_room
                        FROM lecturer_schedules_view AS l
                        WHERE l.employee_id = ?;";
    
                        if ($stmt = mysqli_prepare($connection, $sql)) {
                            
                            mysqli_stmt_bind_param($stmt, "s", $param_employee_id);
                            $param_employee_id = $_SESSION["employee_id"];
    
                            if (mysqli_stmt_execute($stmt)) {
                                
                                mysqli_stmt_store_result($stmt);
                                mysqli_stmt_bind_result($stmt, $db_subject_code, $db_subject_name, $db_class_code, $db_class_room);
    
                                echo '<thead> <tr>
                                    <th> Subject Code </th>
                                    <th> Subject Name </th>
                                    <th> Class </th>
                                    <th> Room </th>
                                    </tr> </tr>
                                </tr> </thead>';
                            
                                echo '<tbody>';
                                
                                while (mysqli_stmt_fetch($stmt)) {
                                    echo '<tr>';
                                    echo '<th>';
                                    echo $db_subject_code;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_subject_name;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_class_code;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_class_room;
                                    echo '</th>';
                                    echo '<th>';
                                    echo '<a href="lecturer.php?subject_code=' . $db_subject_code . '" class="btn btn-danger">Choose</a>';
                                    echo '</th>';
                                    echo '</tr>';
                                }
                                echo '<tbody>';
    
                            } else {
                                echo "Oops! Something went wrong. Please try again later.";
                            }
                            mysqli_stmt_close($stmt);
                        } else {
                            echo "Oops! Something went wrong. Please try again later.";
                        }
                    }
                    
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>