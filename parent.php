<?php
session_start();
 
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once "config.php";

if ($_SESSION["access"] == "parent") {

    $sql = "SELECT p.name, s.student_id
    FROM
        students AS s,
        student_parents AS sp,
        parents AS p
    WHERE
        s.index = sp.student_index AND
        p.index = sp.parent_index AND
        p.user_index IN ( SELECT u.index FROM users AS u WHERE u.username = ? );";

    if ($stmt = mysqli_prepare($connection, $sql)) {
        
        mysqli_stmt_bind_param($stmt, "s", $param_username);
        $param_username = $_SESSION["username"];

        if (mysqli_stmt_execute($stmt)) {
            
            mysqli_stmt_store_result($stmt);
            if (mysqli_stmt_num_rows($stmt) == 1) {

                mysqli_stmt_bind_result($stmt, $db_name, $db_student_id);
                if (mysqli_stmt_fetch($stmt)) {
                    $_SESSION["name"] = $db_name;
                    $_SESSION["student_id"] = $db_student_id;
                }
            } else {
                $_SESSION["loggedin"] = false;
                header("location: login.php");
                exit;
            }
        } else {
            $_SESSION["loggedin"] = false;
            header("location: login.php");
            exit;
        }

        mysqli_stmt_close($stmt);
    } else {
        $_SESSION["loggedin"] = false;
        header("location: login.php");
        exit;
    }

} else {
    if ($_SESSION["access"] == "lecturer") {
        header("location: lecturer.php");
    } else if ($_SESSION["access"] == "staff") {
        header("location: staff.php");
    } else if ($_SESSION["access"] == "student") {
        header("location: student.php");
        exit;
    } else {
        $_SESSION["loggedin"] = false;
        header("location: login.php");
    }
    exit;
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Parent Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body { font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div class="page-header">
        <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["name"]); ?></b></h1>
    </div>

    <p>
        <a href="parent.php" class="btn btn-danger">View Schedules</a>
        <a href="logout.php" class="btn btn-danger">Sign Out</a>
    </p>

    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <?php
                    require_once "config.php";

                    if (!isset($_GET["subject_code"]) || $_GET["subject_code"] == "0") {

                        $sql = "SELECT s.subject_code, s.subject_name, s.class_code, s.class_room
                        FROM student_schedules_view AS s
                        WHERE s.student_id = ?;";

                        if ($stmt = mysqli_prepare($connection, $sql)) {
                            
                            mysqli_stmt_bind_param($stmt, "s", $param_student_id);
                            $param_student_id = $_SESSION["student_id"];

                            if (mysqli_stmt_execute($stmt)) {
                                
                                mysqli_stmt_store_result($stmt);
                                mysqli_stmt_bind_result($stmt, $db_subject_code, $db_subject_name, $db_class_code, $db_class_room);
                                
                                echo '<thead> <tr>
                                    <th> Subject Code </th>
                                    <th> Subject Name </th>
                                    <th> Class Code </th>
                                    <th> Room </th>
                                    <th> </th>
                                </tr> </thead>';

                                echo '<tbody>';
                                while (mysqli_stmt_fetch($stmt)) {
                                    echo '<tr>';
                                    echo '<th>';
                                    echo $db_subject_code;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_subject_name;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_class_code;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_class_room;
                                    echo '</th>';
                                    echo '<th>';
                                    echo '<a href="parent.php?subject_code=' . $db_subject_code . '" class="btn btn-danger">Choose</a>';
                                    echo '</th>';
                                    echo '</tr>';
                                }
                                echo '</tbody>';
                            } else {
                                echo "Oops! Something went wrong. Please try again later.";
                            }
                            mysqli_stmt_close($stmt);
                        } else {
                            echo "Oops! Something went wrong. Please try again later.";
                        }
                    }
                    else {
                        $sql = "SELECT s.subject_code, s.name, c.class_code, sp.week, sp.presence
                        FROM student_presence_view AS sp, subjects AS s, classes AS c
                        WHERE 
                        sp.student_id = ? AND 
                        sp.subject_code = ? AND
                        s.subject_code = sp.subject_code AND 
                        c.class_code = sp.class_code AND
                        s.index = c.subjects_index 
                        ORDER BY sp.week ASC;";

                        

                        if ($stmt = mysqli_prepare($connection, $sql)) {
                            
                            mysqli_stmt_bind_param($stmt, "ss", $param_student_id, $param_subject_code);
                            $param_student_id = $_SESSION["student_id"];
                            $param_subject_code = $_GET["subject_code"];

                            if (mysqli_stmt_execute($stmt)) {
                                
                                mysqli_stmt_store_result($stmt);
                                mysqli_stmt_bind_result($stmt, $db_subject_code, $db_subject_name, $db_class_code, $db_week, $db_presence);
                                
                                echo '<thead> <tr>
                                    <th> Subject Code </th>
                                    <th> Subject Name </th>
                                    <th> Class Code </th>
                                    <th> Week </th>
                                    <th> Presence </th>
                                    <th> </th>
                                </tr> </thead>';

                                echo '<tbody>';
                                while (mysqli_stmt_fetch($stmt)) {
                                    echo '<tr>';
                                    echo '<th>';
                                    echo $db_subject_code;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_subject_name;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_class_code;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_week;
                                    echo '</th>';
                                    echo '<th>';
                                    echo $db_presence;
                                    echo '</th>';
                                    echo '</tr>';
                                }
                                echo '</tbody>';
                            } else {
                                echo "Oops! Something went wrong. Please try again later.";
                            }
                            mysqli_stmt_close($stmt);
                        } else {
                            echo "Oops! Something went wrong. Please try again later.";
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>