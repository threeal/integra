<?php
session_start();
 
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    if (isset($_SESSION["access"]))
    {
        if ($_SESSION["access"] == "lecturer") {
            header("location: lecturer.php");
            exit;
        } else if ($_SESSION["access"] == "staff") {
            header("location: staff.php");
            exit;
        } else if ($_SESSION["access"] == "student") {
            header("location: student.php");
            exit;
        } else if ($_SESSION["access"] == "parent") {
            header("location: parent.php");
            exit;
        } else {
            $_SESSION["loggedin"] = false;
        }
    }
}
 
require_once "config.php";
 
$username = $password = "";
$username_err = $password_err = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
 
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter username.";
    } else {
        $username = trim($_POST["username"]);
    }
    
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter your password.";
    } else {
        $password = trim($_POST["password"]);
    }
    
    if (empty($username_err) && empty($password_err)) {
        
        $sql = "SELECT u.password, u.access FROM users AS u WHERE username = ?";
        
        if ($stmt = mysqli_prepare($connection, $sql)) {
            
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = $username;

            if (mysqli_stmt_execute($stmt)) {

                mysqli_stmt_store_result($stmt);
                
                if (mysqli_stmt_num_rows($stmt) == 1) {

                    mysqli_stmt_bind_result($stmt, $db_password, $db_access);
                    if (mysqli_stmt_fetch($stmt)) {
                        if ($password == $db_password) {
                            session_start();
                            
                            $_SESSION["loggedin"] = true;
                            $_SESSION["username"] = $username;
                            $_SESSION["access"] = $db_access;
                            
                            if ($db_access == "lecturer") {
                                header("location: lecturer.php");
                            } else if ($db_access == "staff") {
                                header("location: staff.php");
                            } else if ($db_access == "student") {
                                $_SESSION["subject_code"] = 'EW184002';
                                header("location: student.php");
                            } else if ($db_access == "parent") {
                                header("location: parent.php");
                            } else {
                                $_SESSION["loggedin"] = false;
                            }
                        } else {
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else {
                    $username_err = "No account found with that username.";
                }
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }
        
            mysqli_stmt_close($stmt);
        }
    }
    
    mysqli_close($connection);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body { font: 14px sans-serif; }
        .wrapper { width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h1>Login</h1>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (empty($username_err)) ? '' : 'has-error'; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (empty($password_err)) ? '' : 'has-error'; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
        </form>
    </div>
</body>
</html>