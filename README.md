# Integra (Tugas BasDat)

- Install [XAMPP](https://www.apachefriends.org/index.html)
- Masuk folder XAMPP (Windows: `c:/xampp` Linux: `/opt/lampp`)
- Git clone (atau extract) `https://gitlab.com/threeal/integra` ke folder `htdocs`
- Buka XAMPP
- Start MySQL dan Apache
- Install [MySQL Workbench](https://www.mysql.com/products/workbench/)
- Buka MySQL Workbench
- Buka `schema.mwb` pada integra
- Synchronize ke server MySQL (hapus semua kata `visible` ketika query)
- Buka `127.0.0.1/phpmyadmin/`
- Import database `users.sql`, `lecturers.sql`, `staff.sql`, `student.sql`, dan `parent.sql`
- Buka `127.0.0.1/login.php`
- Login sesuai user yang dinginkan