<?php
session_start();
 
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once "config.php";

if ($_SESSION["access"] == "staff") {

    $sql = "SELECT s.employee_id, s.name FROM staffs AS s
    WHERE s.user_index IN ( SELECT u.index FROM users AS u WHERE u.username = ? );";

    if ($stmt = mysqli_prepare($connection, $sql)) {
        
        mysqli_stmt_bind_param($stmt, "s", $param_username);
        $param_username = $_SESSION["username"];

        if (mysqli_stmt_execute($stmt)) {
            
            mysqli_stmt_store_result($stmt);
            if (mysqli_stmt_num_rows($stmt) == 1) {

                mysqli_stmt_bind_result($stmt, $db_employee_id, $db_name);
                if (mysqli_stmt_fetch($stmt)) {
                    $_SESSION["employee_id"] = $db_employee_id;
                    $_SESSION["name"] = $db_name;
                }
            } else {
                $_SESSION["loggedin"] = false;
                header("location: login.php");
                exit;
            }
        } else {
            $_SESSION["loggedin"] = false;
            header("location: login.php");
            exit;
        }

        mysqli_stmt_close($stmt);
    } else {
        $_SESSION["loggedin"] = false;
        header("location: login.php");
        exit;
    }

} else {
    if ($_SESSION["access"] == "lecturer") {
        header("location: lecturer.php");
    } else if ($_SESSION["access"] == "student") {
        header("location: student.php");
    } else if ($_SESSION["access"] == "parent") {
        header("location: parent.php");
        exit;
    } else {
        $_SESSION["loggedin"] = false;
        header("location: login.php");
    }
    exit;
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Staff Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body { font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div class="page-header">
        <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["name"]); ?></b>.</h1>
        <h3>NIP. <?php echo htmlspecialchars($_SESSION["employee_id"])?> </h3>
    </div>
    <p>
        <a href="logout.php" class="btn btn-danger">Import Data</a>
        <a href="logout.php" class="btn btn-danger">View Students</a>
        <a href="logout.php" class="btn btn-danger">Sign Out</a>
    </p>
</body>
</html>